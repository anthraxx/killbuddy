use clap::{ArgAction, Parser};

/// An evil helper daemon that nukes naughty accounts.
#[derive(Debug, Parser)]
#[command(author, version, about, long_about = None)]
#[command(propagate_version = true)]
pub struct Args {
    /// Verbose logging, specify twice for more
    #[arg(short, long, action = ArgAction::Count)]
    pub verbose: u8,

    /// Username to kill
    #[arg()]
    pub username: String,
}
